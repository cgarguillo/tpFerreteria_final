from django.shortcuts import render, redirect
from ferreteria.models import Producto, Marca
from ferreteria.helpers.cargas import cargar_produc_basic, cargar_marca_basic, modificar_produc_basic
from django.db import IntegrityError
# Create your views here.

def cargar_marca(request):
    lista_marca = Marca.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            cargar_marca_basic(nombre)
        except Exception as err:
            return render(request, 'marca_carga.html', {'lista_marca': lista_marca,'error': str(err)})
    return render(request, 'marca_carga.html', {'lista_marca': lista_marca})


def cargar_produc(request):
    lista_produc = Producto.objects.all()
    lista_marca = Marca.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            cantidad = request.POST.get('cantidad')
            marca_id = request.POST.get('marca_id')
            cargar_produc_basic(nombre,cantidad,marca_id)
        except IntegrityError as otroErr:
            err = 'HAY UN ERROR BD EN LA PAGINA'
            return render(request, 'lista_produc.html',{'lista_produc': lista_produc, 'lista_marca': lista_marca, 'error': str(otroErr)})
        except Exception as err:
            return render(request, 'lista_produc.html', {'lista_produc': lista_produc, 'lista_marca': lista_marca,'error':str(err)})
    return render(request, 'lista_produc.html', {'lista_produc': lista_produc, 'lista_marca': lista_marca})



def modificar_produc(request, id_produc):
    produc = Producto.objects.get(id=id_produc)
    lista_marca = Marca.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')        ##() va la variable que ingresamos en el html
            cantidad = request.POST.get('cantidad')  ##() va la variable que ingresamos en el html
            marca_id = request.POST.get('marca_id')
            modificar_produc_basic(produc,nombre,cantidad,marca_id) #

        except IntegrityError as otroErr:
            err = 'ErrorBD'
            return render(request, 'modificar_produc.html', {'produc': produc, 'lista_marca': lista_marca,'error': str(otroErr)})

        except Exception as err:
            return render(request, 'modificar_produc.html',{'produc': produc, 'lista_marca': lista_marca, 'error': str(err)})

        return redirect('/producto/cargar_produc')

    return render(request, 'modificar_produc.html', {'produc': produc, 'lista_marca': lista_marca})



def eliminar_produc(request, id_produc):
    produc = Producto.objects.get(id=id_produc).delete()

    return redirect('/producto/cargar_produc')





