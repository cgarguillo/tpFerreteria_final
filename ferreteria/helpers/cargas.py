from ferreteria.models import Marca, Producto     #importamos los models


def cargar_marca_basic(nombre):
    lista_marca = Marca.objects.all()

    for item in lista_marca:
        if item.nombre == nombre:
            raise Exception("El sistema no permite ingresar Marcas duplicadas!!!")
        if nombre == '':
            raise Exception("Error: el nombre no puede estar vacio")

    marca = Marca()
    marca.nombre = nombre
    marca.save()

def cargar_produc_basic(nombre, cantidad, marca_id):
    lista_produc = Producto.objects.all()

    for item in lista_produc:
        if item.nombre == nombre and item.cantidad == int(cantidad) and item.marca_id == int(marca_id):
            raise Exception("Atencion!!!")
        if nombre == '':
            raise Exception("Error: el nombre no puede estar vacio")
        if cantidad == '':
            raise Exception("Error: la cantidad no puede estar vacia")
        if int (cantidad) < int (0):
            raise Exception("Error: la cantidad no puede ser negativa")


    produc = Producto()
    produc.nombre = nombre
    produc.cantidad = cantidad
    produc.marca_id = marca_id
    produc.save()


def modificar_produc_basic(produc, nombre, cantidad, marca_id):  ###############################
    lista_produc = Producto.objects.all()

    for item in lista_produc:
        if item.nombre == nombre and item.cantidad == int(cantidad) and item.marca_id == int(marca_id):
            raise Exception("No se puede duplicar Producto y Cantidad")

    produc.nombre = nombre
    produc.cantidad = cantidad
    produc.marca_id = marca_id
    produc.save()
