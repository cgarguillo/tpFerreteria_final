"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from ferreteria import views as view_ferreteria

urlpatterns = [
    path('admin/', admin.site.urls),
    path('producto/cargar_produc', view_ferreteria.cargar_produc),
    path('producto/cargar_produc/<int:id_produc>', view_ferreteria.modificar_produc),
    path('producto/cargar_marca', view_ferreteria.cargar_marca),
    path('producto/cargar_produc/<int:id_produc>', view_ferreteria.eliminar_produc),

]